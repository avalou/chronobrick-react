describe("Connecting to the API", () => {
	it("Displays the Login page component", () => {
		cy.visit("http://localhost:3000/login");

		cy.get("[id='email']").type("lou01@email.com");

		cy.get("[id='password']").type("azerty");

		cy.get("[id='submitButton']").click();

		cy.contains("Utilisateurice connecté·e, bravo !");
	});
});
