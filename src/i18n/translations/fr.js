export const fr = {
	home_connected: "Utilisateurice connecté·e - lol",
	home_notConnected: "Utilisateurice non connecté·e - lol",
	navbar_login_btn: "Se connecter",
	navbar_signup_btn: "S'inscrire",
	navbar_logout_btn: "Déconnexion",
	navbar_profil_btn: "Profile",
	loginForm_email_label: "Adresse email",
	loginForm_password_label: "Mot de passe"
};
