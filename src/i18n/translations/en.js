export const en = {
	home_connected: "User is connected",
	home_notConnected: "User is not connected",
	navbar_login_btn: "Login",
	navbar_signup_btn: "Signup",
	navbar_logout_btn: "Logout",
	navbar_profil_btn: "Profil",
	loginForm_email_label: "Email",
	loginForm_password_label: "Password"
};
