import { LOCALES } from "./locales";

import { en } from "./translations/en";
import { fr } from "./translations/fr";

export const messages = {
	[LOCALES.ENGLISH]: en,
	[LOCALES.FRENCH]: fr
};
