import axios from 'axios';
import { BASE_URL } from "./config.js";

const get = async (
  endpoint,
  jwt_token = null,
  header = { "Content-Type": "application/json" }) => {

  let opt = header;
  if (jwt_token){
      opt["Authorization"] = jwt_token
  }

  try {
    console.log("HELLO 1");
    const response = await axios.get(BASE_URL + endpoint, { headers: opt })
    console.log(response.data);
    return response.data
  } catch (err) {
    console.error(`An error occurred while trying to fetch` + endpoint + `:` + err);
  }
}

export default get;