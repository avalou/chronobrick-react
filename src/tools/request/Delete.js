import axios from 'axios';
import { BASE_URL } from "./config.js";

const del = async (
  endpoint,
  jwt_token = null,
  header = { "Authorization": jwt_token }) => {

  try {
    const response = await axios.delete(BASE_URL + endpoint, { headers: header })
    return response
  } catch (err) {
    console.error(`An error occurred while trying to fetch ${endpoint}. ${err}`);
  }
}

export default del;