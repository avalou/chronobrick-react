import axios from 'axios';
import { BASE_URL } from "./config.js";

const post = async (
  endpoint,
  body = null,
  jwt_token = null,
  header = { "Content-Type": "application/json" }) => {

  let opt = header;
  if (jwt_token){
      opt["Authorization"] = jwt_token
  }

  try {
    return await axios.post(BASE_URL + endpoint, body, { headers: opt })
  } catch (err) {
    console.error(`An error occurred while trying to fetch ${endpoint}. ${err}`);
  }
}

export default post;