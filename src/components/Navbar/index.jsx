import React from "react";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import { FormattedMessage } from "react-intl";

import "./index.scss";
import Star from "../../assets/img/star.ico";
import LogoutButton from "../buttons/LogoutButton";
import { LOCALES } from "../../i18n/locales";

const Navbar = ({ handleLocaleChange, currentLocale }) => {
	const currentUser = useSelector((state) => state.currentUser);

	const languages = [
		{ name: "Français", code: LOCALES.FRENCH },
		{ name: "English", code: LOCALES.ENGLISH }
	];
	return (
		<div className="navbar">
			<div className="navbar__LeftSide">
				<Link to="/">
					<img
						src={Star}
						className="navbar__Icon"
						alt="Temporary icon"
						width="25"
						height="25"
					/>
					<p className="navbar__BrandName">Chronobriq</p>
				</Link>
			</div>
			<div className="navbar__RightSide">
				<div className="languageSwitcher">
					<select onChange={handleLocaleChange} value={currentLocale}>
						{languages.map(({ name, code }) => (
							<option key={code} value={code}>
								{name}
							</option>
						))}
					</select>
				</div>
				{!currentUser && (
					<>
						<Link to="/signup">
							<button>
								<FormattedMessage
									id="navbar_signup_btn"
									defaultMessage="S'inscrire"
									description="Signup button"
								/>
							</button>
						</Link>
						<Link to="/login">
							<button>
								<FormattedMessage
									id="navbar_login_btn"
									defaultMessage="Se connecter"
									description="Login button"
								/>
							</button>
						</Link>
					</>
				)}
				{currentUser && (
					<>
						<Link to="/me">
							<button>
								<FormattedMessage
									id="navbar_profil_btn"
									defaultMessage="Profile"
									description="Profil button"
								/>
							</button>
						</Link>
						<LogoutButton />
					</>
				)}
			</div>
		</div>
	);
};

export default Navbar;
