import React from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import Cookies from "js-cookie";
import { removeCurrentUser } from "../../../redux/action/index";

const LogoutButton = () => {
	const dispatch = useDispatch();
	const history = useHistory();

	const handleLogout = (event) => {
		event.preventDefault();
		Cookies.remove("token");
		dispatch(removeCurrentUser());
		localStorage.removeItem("persist:root");
		history.push("/");
	};

	return (
		<button onClick={handleLogout}>
			<FormattedMessage
				id="navbar_logout_btn"
				defaultMessage="Déconnexion"
				description="Logout button"
			/>
		</button>
	);
};

export default LogoutButton;
