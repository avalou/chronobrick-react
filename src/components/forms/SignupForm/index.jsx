import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import post from "../../../tools/request/Post";
import "./index.scss";

const SignupForm = () => {
	const [userData, setUserData] = useState(null);
	const [passwordCheck, setPasswordCheck] = useState(null);

	const history = useHistory();

	const handleSubmit = (event) => {
		event.preventDefault();
		const formdata = new FormData(event.currentTarget);
		let pwd = formdata.get("password");
		let c_pwd = formdata.get("confirm_password");
		let username = formdata.get("username");
		let email = formdata.get("email");
		if (pwd !== c_pwd) {
			setPasswordCheck(false);
		} else if (pwd === c_pwd) {
			setPasswordCheck(true);
		}
		setUserData({
			...userData,
			username,
			email,
			pwd
		});
	};

	const handleSignup = async ({ email, username, pwd }) => {
		let body = {
			user: {
				email: email,
				username: username,
				password: pwd
			}
		};
		return await post("/users", body);
	};

	useEffect(async () => {
		if (passwordCheck === false) {
			console.log("Passwords do not match"); // À afficher autrement
		} else if (passwordCheck === true && userData) {
			const response = await handleSignup(userData);
			if (response.status && response.status === 201) {
				history.push({
					pathname: "/",
					state: { status: "201 : user created" }
				});
			}
		}
	}, [passwordCheck, userData]);

	return (
		<div className="signupForm">
			<form action="" onSubmit={handleSubmit}>
				<label htmlFor="username">Nom d'utilisateur</label>
				<input type="text" id="username" name="username" />
				<label htmlFor="email">Adresse email</label>
				<input type="email" id="email" name="email" />
				<label htmlFor="password">Mot de passe</label>
				<input type="password" id="password" name="password" />
				<label htmlFor="confirm_password">Confirmation du mot de passe</label>
				<input type="password" id="confirm_password" name="confirm_password" />
				<button type="submit">Valider</button>
			</form>
		</div>
	);
};

export default SignupForm;
