import React, { useState, useEffect } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { FormattedMessage } from "react-intl";
import jwt_decode from "jwt-decode";
import Cookies from "js-cookie";

import post from "../../../tools/request/Post";
import { setCurrentUser } from "../../../redux/action";
import "./index.scss";

const LoginForm = () => {
	const [userData, setUserData] = useState(null);
	const [inputEmail, setInputEmail] = useState("");
	const [inputPassword, setInputPassword] = useState("");

	const dispatch = useDispatch();
	const history = useHistory();

	const handleEmailChange = (e) => {
		setInputEmail(e.target.value);
	};

	const handlePasswordChange = (e) => {
		setInputPassword(e.target.value);
	};

	const handleSubmit = (e) => {
		e.preventDefault();
		setUserData({
			...userData,
			email: inputEmail,
			password: inputPassword
		});
	};

	const handleLogin = async (data) => {
		// Écrire une fonction pour vérifier plus précisément que data
		// contient bien les données nécessaires
		if (data) {
			let body = {
				user: { email: data.email, password: data.password }
			};
			const response = await post("/tokens", body);
			// Pareil ici, utiliser une fonction qui vérifie la validité des données
			if (response != null) {
				Cookies.set("token", response.data.token, {
					sameSite: "strict",
					Secure: true
				});
				const payload = {
					type: "user",
					id: jwt_decode(response.data.token).user_id,
					email: userData.email,
					username: response.data.username
				};
				dispatch(setCurrentUser(payload));
				history.push("/");
			}
		}
	};

	useEffect(() => {
		handleLogin(userData);
	}, [userData]);

	return (
		<div className="LoginForm">
			<form action="" onSubmit={handleSubmit}>
				<label htmlFor="email">
					<FormattedMessage
						id="loginForm_email_label"
						defaultMessage="Adress email"
						description="Login form email address label"
					/>
				</label>
				<input
					type="email"
					id="email"
					name="email"
					value={inputEmail}
					onChange={handleEmailChange}
				/>

				<label htmlFor="password">
					<FormattedMessage
						id="loginForm_password_label"
						defaultMessage="Mot de passe"
						description="Login form password label"
					/>
				</label>
				<input
					type="password"
					id="password"
					name="password"
					value={inputPassword}
					onChange={handlePasswordChange}
				/>

				<button type="submit" id="submitButton">
					Valider
				</button>
			</form>
		</div>
	);
};

export default LoginForm;
