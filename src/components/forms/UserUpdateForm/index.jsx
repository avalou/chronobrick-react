import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import Cookies from "js-cookie";

import update from "../../../tools/request/Update";
import { setCurrentUser } from "../../../redux/action";

const UserUpdateForm = () => {
	const [userData, setUserData] = useState(null);
	const currentUser = useSelector((state) => state.currentUser);
	const dispatch = useDispatch();
	const history = useHistory();

	const handleSubmit = (e) => {
		e.preventDefault();
		const formdata = new FormData(e.currentTarget);
		if (formdata.get("email") !== "" && formdata.get("username") !== "") {
			const email = formdata.get("email");
			const username = formdata.get("username");
			setUserData({
				...userData,
				username: username,
				email: email
			});
		} else if (
			formdata.get("email") !== "" &&
			formdata.get("username") === ""
		) {
			const email = formdata.get("email");
			const username = currentUser.username;
			setUserData({
				...userData,
				username: username,
				email: email
			});
		} else if (
			formdata.get("email") === "" &&
			formdata.get("username") !== ""
		) {
			const email = currentUser.email;
			const username = formdata.get("username");
			setUserData({
				...userData,
				username: username,
				email: email
			});
		}
	};

	const handleUpdate = async ({ username, email }) => {
		const token = Cookies.get("token");
		const body = { user: { username: username, email: email } };
		const response = await update(`/users/${currentUser.id}`, body, token);
		console.log(response);
		if (response.status === 200) {
			handleChange(response);
		}
	};

	const handleChange = (res) => {
		console.log("handle change : " + res.data.attributes);
		const payload = {
			type: "user",
			id: res.data.data.id,
			email: res.data.data.attributes.email,
			username: res.data.data.attributes.username
		};
		console.log("Payload : " + payload);
		dispatch(setCurrentUser(payload));
		history.push("/me");
	};

	useEffect(() => {
		if (userData !== null) {
			handleUpdate(userData);
		}
	}, [userData]);

	return (
		<form action="" onSubmit={handleSubmit}>
			<label htmlFor="username">Nom d'utilisateur</label>
			<input type="text" id="username" name="username" />
			<label htmlFor="email">Adresse email</label>
			<input type="email" id="email" name="email" />
			<button type="submit">Valider</button>
		</form>
	);
};

export default UserUpdateForm;
