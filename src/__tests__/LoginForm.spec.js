import { render, screen } from "../tools/test-utils";
// import userEvent from "@testing-library/user-event";

import LoginForm from '../components/forms/LoginForm';

describe('<LoginForm />', () => {
  beforeEach(() => {
    render(<LoginForm />)
  });
  describe('clicking the validate button', () => {
    it("should display page title", () => {
      expect(screen.getByText("Adresse email")).toBeInTheDocument();
      expect(screen.getByText("Mot de passe")).toBeInTheDocument();
    })
  });
});