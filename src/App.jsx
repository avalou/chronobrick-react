import React, { useState } from "react";
import {
	BrowserRouter as Router,
	Route,
	Switch,
	Redirect
} from "react-router-dom";
import { useSelector } from "react-redux";
import { IntlProvider } from "react-intl";

import { LOCALES } from "./i18n/locales";
import { messages } from "./i18n/messages";

import Navbar from "./components/Navbar/";
import Home from "./pages/Home/";
import SignupPage from "./pages/Signup";
import LoginPage from "./pages/Login/";
import ProfilePage from "./pages/Profile";

const App = () => {
	const getInitialLocal = () => {
		const savedLocale = localStorage.getItem("locale");
		return savedLocale || LOCALES.FRENCH;
	};

	const [currentLocale, setCurrentLocale] = useState(getInitialLocal());
	const currentUser = useSelector((state) => state.currentUser);

	const handleLocaleChange = (e) => {
		setCurrentLocale(e.target.value);
		localStorage.setItem("locale", e.target.value);
	};

	const UnAuthRoute = ({ component: Component, ...rest }) => (
		<Route
			{...rest}
			render={(props) =>
				currentUser ? (
					<Redirect to={{ pathname: "/" }} />
				) : (
					<Component {...props} />
				)
			}
		/>
	);

	const AuthRoute = ({ component: Component, ...rest }) => (
		<Route
			{...rest}
			render={(props) =>
				currentUser ? (
					<Component {...props} />
				) : (
					<Redirect to={{ pathname: "/signin" }} />
				)
			}
		/>
	);

	return (
		<IntlProvider
			messages={messages[currentLocale]}
			locale={currentLocale}
			defaultLocale={LOCALES.FRENCH}>
			<Router>
				<Navbar
					currentLocale={currentLocale}
					handleLocaleChange={handleLocaleChange}
				/>
				<section className="app">
					<Switch>
						<Route exact path="/">
							<Home />
						</Route>
						<UnAuthRoute path="/login" component={LoginPage} />
						<UnAuthRoute path="/signup" component={SignupPage} />
						<AuthRoute path="/me" component={ProfilePage} />
					</Switch>
				</section>
			</Router>
		</IntlProvider>
	);
};

export default App;
