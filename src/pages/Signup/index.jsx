import React from "react";
import "./index.scss";
import SignupForm from "../../components/forms/SignupForm";

const SignupPage = () => {
	return (
		<div className="SignupPage">
			<h2>Page d'inscription</h2>
			<SignupForm />
		</div>
	);
};

export default SignupPage;
