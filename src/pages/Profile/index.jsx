import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import Cookies from "js-cookie";

import del from "../../tools/request/Delete";
import { removeCurrentUser } from "../../redux/action/index";
import UserUpdateForm from "../../components/forms/UserUpdateForm";

const ProfilePage = () => {
	const [deleteSuccess, setDeleteSuccess] = useState(null);
	const [settings, setSettings] = useState(false);
	const currentUser = useSelector((state) => state.currentUser);

	const dispatch = useDispatch();
	const history = useHistory();

	const handleDelete = async () => {
		let token = Cookies.get("token");
		const response = await del(`/users/${currentUser.id}`, token);
		if (response.status === 204) {
			setDeleteSuccess(true);
		} else if (response.status === 403) {
			setDeleteSuccess(false);
		}
	};

	const toogleSettings = () => {
		if (settings === false) {
			setSettings(true);
		} else {
			setSettings(false);
		}
	};

	useEffect(() => {
		console.log(currentUser);
		if (deleteSuccess && deleteSuccess === true) {
			Cookies.remove("token");
			dispatch(removeCurrentUser());
			localStorage.removeItem("persist:root");
			history.push({
				pathname: "/",
				state: { status: "204 : user deleted" }
			});
		}
	}, [deleteSuccess]);

	return (
		<div>
			<h1>Hello {currentUser.username}</h1>
			<button onClick={handleDelete}>Supprimer le compte</button>
			<button onClick={toogleSettings}>Mettre à jour les informations</button>
			{settings === true ? (
				<div className="truc">
					<h2>Modifiez vos informations :</h2>
					<UserUpdateForm />
				</div>
			) : (
				<> </>
			)}
		</div>
	);
};

export default ProfilePage;
