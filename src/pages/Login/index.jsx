import React from "react";
import LoginForm from "../../components/forms/LoginForm/";

const LoginPage = () => {
	return (
		<div className="LoginPage">
			<h2>Page de connexion</h2>
			<LoginForm />
		</div>
	);
};

export default LoginPage;
