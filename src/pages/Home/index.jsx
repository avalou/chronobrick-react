import React, { useEffect } from "react";
import { useLocation } from "react-router";
import { useSelector } from "react-redux";
import { FormattedMessage } from "react-intl";

const Home = () => {
	const currentUser = useSelector((state) => state.currentUser);

	const location = useLocation();

	return (
		<div className="test">
			<h2 className="text-red-400">Home</h2>
			{currentUser && (
				<div>
					<p>
						<FormattedMessage
							id="home_connected"
							defaultMessage="Utilisateurice connecté·e - {what}"
							description="verification header - connected"
							values={{ what: "default msg" }}
						/>
					</p>
				</div>
			)}
			{!currentUser && (
				<div>
					<p>
						<FormattedMessage
							id="home_notConnected"
							defaultMessage="Utilisateurice non connecté·e - {what}"
							description="verification header - not connected"
							values={{ what: "default msg" }}
						/>
					</p>
				</div>
			)}
			<p>{location.state && location.state.status}</p>
		</div>
	);
};

export default Home;
