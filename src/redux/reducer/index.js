const initialState = { currentUser: null };

const reducer = (state = initialState, action) => {
	switch (action.type) {
		case "SET_CURRENT_USER":
			return { currentUser: action.payload };
		case "REMOVE_CURRENT_USER":
			return initialState;
		default:
			return { ...state };
	}
};

export default reducer;
