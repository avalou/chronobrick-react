const setCurrentUser = (currentUser) => {
	return {
		type: "SET_CURRENT_USER",
		payload: currentUser
	};
};

const removeCurrentUser = () => {
	return {
		type: "REMOVE_CURRENT_USER"
	};
};

export { setCurrentUser, removeCurrentUser };
